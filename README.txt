
INTRODUCTION
------------

The Media: thePlatform mpx features module allow administrator to export 
thePlatform MPX account level settings to a feature. 

The settings includes: 
  default_player
  account_pid
  import_account

Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/media_theplatform_mpx_features


-- REQUIREMENTS --

* Media: thePlatform mpx


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will provide an option to export mpx account settings to 
feature which were enabled in mpx_accounts.
