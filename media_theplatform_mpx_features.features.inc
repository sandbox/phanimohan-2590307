<?php
/**
 * @file
 * Feature implements for media_theplatform_mpx.
 */

/**
 * Implements hook_features_export_options().
 */
function mpx_account_features_export_options() {
  foreach (_media_theplatform_mpx_get_account_data() as $account) {
    $player_select = media_theplatform_mpx_get_players_select($account->id);
    if (empty($player_select)) {
      continue;
    }
    $player_select = reset($player_select);
    $component_name = $account->id . '--' . drupal_html_id($account->import_account);
    $options['mpx-account--' . $component_name] = t('@account - @player', array(
      '@account' => urldecode($account->import_account),
      '@player' => $player_select[$account->default_player],
    ));
  }

  return $options;
}

/**
 * Implements hook_features_export().
 */
function mpx_account_features_export($data, &$export, $module) {
  $export['dependencies']['media_theplatform_mpx_features'] = 'media_theplatform_mpx_features';

  foreach ($data as $component) {
    $export['features']['mpx_account'][$component] = $component;
  }
  return array();
}

/**
 * Implements hook_features_export_render().
 */
function mpx_account_features_export_render($module, $data, $export = NULL) {
  $code = array();
  $code[] = '  $mpx_account = array();';
  $code[] = "";

  foreach ($data as $name) {
    $data_options = media_theplatform_mpx_features_component_load($name);
    $component_name = $data_options['id'] . '--' . $data_options['import_account_id'];
    $code[] = "  \$mpx_account['mpx-account--{$component_name}'] = " . features_var_export($data_options, "  ") . ";";
  }
  $code[] = "  return \$mpx_account;";
  $code = implode("\n", $code);
  return array('mpx_account_defaults' => $code);
}

/**
 * Load MPX Account component with default player id.
 *
 * @param string $name
 *   A string that represents mpx account component.
 *
 * @return array
 *   An array with mpx account default details.
 */
function media_theplatform_mpx_features_component_load($name) {
  if (empty($name)) {
    return NULL;
  }

  list(, $account_id, $account_html_id) = explode("--", $name);
  $account = _media_theplatform_mpx_get_account_data($account_id);
  $player_select = media_theplatform_mpx_get_players_select($account_id);
  if (empty($player_select)) {
    return array();
  }

  return array(
    'id' => $account_id,
    'import_account' => $account->import_account,
    'account_pid' => $account->account_pid,
    'default_player' => $account->default_player,
    'import_account_id' => $account_html_id,
  );
}

/**
 * Save default player id details into mpx_accounts.
 *
 * @param array $component
 *   An array that represents MPX account component.
 *
 * @return int
 *   An integer with updated records count.
 */
function media_theplatform_mpx_features_account_save(array $component) {
  $num_rows_updated = db_merge('mpx_accounts')
    ->key(array('id' => $component['id']))
    ->fields(array(
      'account_pid' => $component['account_pid'],
      'default_player' => $component['default_player'],
    ))
    ->execute();

  return $num_rows_updated;
}

/**
 * Implements hook_features_revert().
 */
function mpx_account_features_revert($module) {
  mpx_account_features_rebuild($module);
}

/**
 * Implements hook_features_rebuild().
 */
function mpx_account_features_rebuild($module) {
  if ($defaults = features_get_default('mpx_account', $module)) {
    foreach ($defaults as $format) {
      media_theplatform_mpx_features_account_save($format);
    }
  }
}
